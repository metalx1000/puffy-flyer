#!/bin/bash

array="[ ";
for m in *.mp3;
do
  ffmpeg -y -i "$m" "${m%%.mp3}.ogg"
  array+="\"${m%%.mp3}\", "
done
array+=" ]"

output="$(echo -e "var sounds = $array"|sed 's/,  / /g')"
echo "$output"
echo "Copied to Clipboard"
echo "$output"|xclip
