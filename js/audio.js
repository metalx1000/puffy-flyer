function musicCreate(){
  music = scene.sound.add('music');
  music.play();
  music.loop = true;

  if (typeof(Storage) !== "undefined") {
    if (localStorage.musicVol) {
      var volControl = document.getElementById("musicVol");
      volControl.value = localStorage.musicVol;
      music.volume = localStorage.musicVol;
    }
  }
}

function music_setVol(e){
  var val = e.value;
  music.volume = val;
  if (typeof(Storage) !== "undefined") {
    localStorage.setItem("musicVol", val);
  }
}
