const config = {
  type: Phaser.AUTO,
  parent: "game",
  width: 960,
  height: 540,
  scene: {
    preload: preload,
    update: update,
    create: create
  },
  scale: {
    mode: Phaser.Scale.FIT,
    parent: 'phaser-example',
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: 960,
    height: 540
  },
  physics: {
    default: 'arcade',
      arcade: {
        gravity: {
          y: 300
        },
      }
  }
};

const game = new Phaser.Game(config);
let scene,platforms;
let enemies;
let width = game.config.width;
let height = game.config.height;

function preload (){
  scene=this;
  loadScreen("Loading Puffy Flyer...");
  // map made with Tiled in JSON format
  scene.load.tilemapTiledJSON('map', 'assets/maps/level2.json?=3');
  // tiles in spritesheet 
  scene.load.image('tiles', 'assets/tiles/tiles.png');
  //Load Sprites
  playerPreload();
  enemyPreload();

  //UI
  uiPreload();

  //this.load.sprite('puff', 'assets/sprites/puff.png');

  this.load.audio('music', ['assets/music/music_2.ogg', 'assets/music/music_2.mp3']);

}

function create (){
  enemies = scene.physics.add.group();
  players = scene.physics.add.group();
  hud = scene.add.group();
  musicCreate();

  platforms = loadMap(scene);
  //player = createPlayer();
  //spike = createEnemy(spike);
  //spike.x = width;
  //spike.y = 800;
  //ice = createEnemy(iceblock);
  //ice.x = width/3;

  button = btn_fullscreen();
  btn_fly();
  cursors = this.input.keyboard.createCursorKeys();
}

function update(){
  players.children.iterate(function (player) {
    player.update();
  });

  enemies.children.iterate(function (enemy) {
    enemy.update();
  });

}

function createCamera(follow){
  // set bounds so the camera won't go outside the game world
  scene.cameras.main.setBounds(0, 0, platforms.widthInPixels, platforms.heightInPixels);

  // set background color, so the sky is not black    
  scene.cameras.main.setBackgroundColor('#ccccff'); 
}
