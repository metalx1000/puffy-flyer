spike = {
  sprite : 'spike',
  speed : 80,
  scale : .7,
  bounce : .2
};

iceblock = {
  sprite : 'ice-block',
  speed : 80,
  scale : .8,
  rturn : true,
  bounce : .2
};

hippo = {
  sprite : 'hippo',
  speed : 100,
  scale : .8,
  rturn : true,
  bounce : .2
};

//If rturn (random turn) set 
function RTURN(enemy){
  var turn_time = Math.floor(Math.random() * 3);
  if(turn_time == 1){
    enemy.speed = enemy.speed * -1;
  }
}

function createEnemy(sprite,config){
  //var player = scene.physics.add.sprite(game.config.width / 2, game.config.height / 2, 'walk');
  //var enemy = scene.physics.add.sprite(game.config.width / 2, game.config.height / 2, config.sprite);
  var enemy = enemies.create(sprite.x, sprite.y, "spike");
  enemy.x = sprite.x;
  enemy.y = sprite.y;
  e = enemy;
  v=sprite;
  //  .physics.add.existing

  //use 'config' JSON to set enemy config
  for (key in config) {
    if(config.hasOwnProperty(key)){
      enemy[key] = config[key];
    }
  }

  //Enemy turns randomly
  if(enemy.rturn){
    var turn_time = Math.random() * (10000 - 3000) + 3000;
    //scene.time.delayedCall(turn_time, RTURN, [enemy], this);
    scene.time.addEvent({ delay: turn_time, callback: RTURN, args: [enemy], callbackScope: this, loop: true });
  }
  enemy.setTexture(enemy.sprite);
  //  enemy.setBounce(enemy.bounce);

  scene.physics.add.collider(platforms, enemy);

  //UPDATE
  enemy.update = enemyUpdate;
  return enemy;
}

function enemyUpdate(){
  var enemy = this;
  if(enemy.body.onWall()){
    enemy.speed = enemy.speed * -1;
  }

  if(enemy.speed < 1){
    enemy.flipX = false;
  }else{
    enemy.flipX = true;
  }
  enemy.setVelocityX(enemy.speed);  
  //enemy.x = Phaser.Math.Wrap(enemy.x, 0, game.renderer.width);
  //enemy.y = Phaser.Math.Wrap(enemy.y, 0, game.renderer.width);
}

function enemyPreload(){
  var sprites = ["spike","ice-block","hippo"];
  for(s of sprites){
    scene.load.spritesheet(s, 'assets/sprites/'+s+'.png', { frameWidth: 64, frameHeight: 64 });
  }

}
