
function loadMap(game){
  map = game.make.tilemap({ key: 'map' });
  var tileset = map.addTilesetImage('level', 'tiles');
  platforms = map.createDynamicLayer('platforms', tileset, 1, 0);
  platforms.setCollisionByExclusion([-1]);

  sprite_layer = map.getObjectLayer('sprites')['objects'];
  for(s of sprite_layer){
      s.x = (platforms.layer.x - s.x)*-1;
      s.y = (platforms.layer.y - s.y)*-1;
    if(s.type == "player_start"){
      createPlayer(s); 
    }else if(s.type == "ice-block"){
      createEnemy(s,iceblock); 
    }else if(s.type == "spike"){
      createEnemy(s,spike); 
    }else if(s.type == "hippo"){
      createEnemy(s,hippo); 
    }
  }

  return platforms;
}
