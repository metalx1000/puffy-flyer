function playerPreload(){
  var sounds = [ "fly", "go_1", "hit_1", "hit_2", "hit_3" ];

  for(s of sounds){
    scene.load.audio(s, ['assets/sounds/player/'+s+'.ogg', 'assets/sounds/player/'+s+'.mp3']);
  }

  var sprites = ["walk","puff"];
  for(s of sprites){
    scene.load.spritesheet(s, 'assets/sprites/'+s+'.png', { frameWidth: 64, frameHeight: 64 });
  }
}

function createPlayer(sprite){
  //var player = scene.physics.add.sprite(game.config.width / 2, game.config.height / 2, 'walk');
  player = players.create(sprite.x,sprite.y, 'walk');
//  combineJSON(player,sprite);
  //use 'config' JSON to set enemy config
  for (key in sprite) {
    if(sprite.hasOwnProperty(key)){
      player[key] = sprite[key];
    }
  }

  player.startx = player.x;
  player.starty = player.y;

  player.speed = 180;
  player.setScale(.5);
  player.healthText = scene.add.text(16, game.config.height * .9, 'Health: ' + player.health, { fontSize: '32px', fill: '#000' });
  player.healthText.setScrollFactor(0); 
  playerSounds(player);

  player.keys = scene.input.keyboard.addKeys('A,S');
  playerSpawn(player);
  //load player animations
  playerAnimations();
  playerPhysics(player);

  player.update = playerUpdate;

  createCamera(player);
  return player;
}

function playerUpdate(){
  var player = this;
  if(!player.death){
    playerControls(player);
  }else{
    //IF PLAYER DEAD
    playerDead(player);
  }

  if(player.health <= 0){
    player.death=true;
  }

  player.healthText.setText('Health: ' + player.health);
}
//END PLAYER UPDATE
function playerPhysics(player){
  scene.physics.add.overlap(player, enemies, playerHit, null, this);
  //player.setCollideWorldBounds(true);
  player.setBounce(.2);
  scene.physics.add.collider(platforms, player);

}

function playerControls(player){
  key=player.keys;
  //player.x = Phaser.Math.Wrap(player.x, 0, game.renderer.width);
  //player.y = Phaser.Math.Wrap(player.y, 0, game.renderer.height);
  //if (cursors.right.isDown || game.input.pointers[0].isDown){
    var pointer = scene.input.activePointer;
    if(pointer.isDown){
      if(pointer.worldX > player.x + 10){
        playerWalk(player,1);
      }else if(pointer.worldX < player.x - 10){
        playerWalk(player,-1);
      }
    }else if (cursors.right.isDown){
      playerWalk(player,1);
    }else if (cursors.left.isDown){
      playerWalk(player,-1);
    }else{
      player.setVelocityX(0);
    }
    //fly on arrow up or 'a' key
    if (cursors.up.isDown || key.A.isDown){

      player.setVelocityY(-player.speed);
      player.fly = true;
      player.anims.play('puff', true);
    }

    //turn player around when key is pressed
    if(key.S.isDown){
      player.toggleFlipX()
    }

    if (cursors.down.isDown){
      //player.setVelocityY(180);
      //player.body.checkCollision.down=false;
      player.fly = false;
      player.anims.play('walk',true);
    }else{
      //player.body.checkCollision.down=true;
    }

    if(!player.fly){
      //if player is not flying fade fly sound out
      //then stop sound
      if(player.snd_fly.volume > 0){
        player.snd_fly.volume-=.01;
      }else{
        player.snd_fly.stop();
      }
    }
}

function playerDead(player){
  player.body.checkCollision.down=false;
  player.rotation+=.1;
  player.setVelocityY(180);
  player.setVelocityX(0);
  player.alpha = .5;

  // make the camera not follow the player
  scene.cameras.main.stopFollow(player);
}

function playerInvince(player){
  player.alpha = 1;
  player.invince = false;
}

function playerHit(player,enemy){
  if(!player.invince){
    player.alpha = .5;
    player.setVelocity(-200);
    player.invince = true;
    player.health -= 1;
    scene.time.delayedCall(1000, playerInvince, [player], this);

    //play random sound
    var randomSnd = player.snd_hit[Math.floor(Math.random()*player.snd_hit.length)];
    randomSnd.volume = .5;
    randomSnd.play();

    if(player.health == 0){
      scene.time.delayedCall(2000, playerSpawn, [player], this);
    }
  }
}

function playerWalk(player,direction){
  var pointer = scene.input.activePointer;
  if(direction == 1){
    player.flipX = false;
  }else{
    player.flipX = true;

  }
  player.setVelocityX(player.speed * direction);
  if(!player.fly){
    player.anims.play('walk', true);
  }else if(player.fly){
    //play flying sound if not already playing
    if(!player.snd_fly.isPlaying){
      player.snd_fly.volume = 1;
      player.snd_fly.play();
    }
    if(pointer.worldY > player.y + 10){
      player.setVelocityY(player.speed);
    }else if(pointer.worldY < player.y - 10){
      player.setVelocityY(player.speed * -1);
    }
    player.anims.play('puff', true);
  }

}

function playerSpawn(player){
  player.health = 3;
  player.death = false;
  player.alpha = 1;
  player.rotation = 0;
  player.y = player.starty; 
  player.x = player.startx; 
  player.body.checkCollision.down=true;

  player.snd_start.volume=.5;
  player.snd_start.play();

  // make the camera follow the player
  scene.cameras.main.startFollow(player);

}

function playerAnimations(){
  scene.anims.create({
    key: 'puff',
    frames: scene.anims.generateFrameNumbers('puff'),
    frameRate: 10,
    repeat: 0
  });

  scene.anims.create({
    key: 'walk',
    frames: scene.anims.generateFrameNumbers('walk'),
    frameRate: 10,
    repeat: 0
  });

}

function playerSounds(player){
  //Start up sound
  player.snd_start = scene.sound.add('go_1');

  //Fly sound
  player.snd_fly = scene.sound.add('fly');
  player.snd_fly.loop=true;

  //Hit/Hurt Sounds
  player.snd_hit = [];
  player.snd_hit.push(scene.sound.add('hit_1'));
  player.snd_hit.push(scene.sound.add('hit_2'));
  player.snd_hit.push(scene.sound.add('hit_3'));
}
